package alwanassyauqi_rifqiyuner_istamarsiddiq.todolistapp;

import static org.junit.Assert.*;

import org.junit.Test;

public class addtasktest {

    @Test
    public void CheckListTaskIsEmptyOrNot() {
        addtask classUnderTest = new addtask();
        String Task = "Coba";
        classUnderTest.addnewtask(Task);
        boolean expected = false;
        assertEquals(expected, classUnderTest.IsTaskEmpty());
    }

    @Test
    public void AddTaskValid() {
        addtask classUnderTest = new addtask();
        String Task = "Coba";
        String expected = "Coba";
        assertEquals(expected, classUnderTest.addnewtask(Task));
    }

    @Test
    public void AddTaskValidButAlreadyAdded() {
        addtask classUnderTest = new addtask();
        String Task = "Coba";
        classUnderTest.addnewtask(Task);
        String expected = "Task sudah ada";
        assertEquals(expected, classUnderTest.addnewtask(Task));
    }

    @Test
    public void AddEmptyTask() {
        addtask classUnderTest = new addtask();
        String Task = "";
        String expected = "Task Is Empty";
        assertEquals(expected, classUnderTest.addnewtask(Task));
    }
}
