package alwanassyauqi_rifqiyuner_istamarsiddiq.todolistapp;

import java.util.*;

public class addtask {
    LinkedList<String> ListTask = new LinkedList<String>();

    public String addnewtask(String Task) {
        if (Task.isEmpty()) return "Task Is Empty";
        boolean status = ListTask.contains(Task);
        if (status) return "Task sudah ada";
        else ListTask.add(Task);
        return ListTask.peekLast();
    }

    public boolean IsTaskEmpty() {
        return ListTask.isEmpty();
    }
}
